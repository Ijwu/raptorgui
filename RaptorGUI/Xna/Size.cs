﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Xna.Framework
{
    public class Size
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public Size(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public static bool operator >(Size my, Size other)
        {
            return (my.Width > other.Width && my.Height > other.Height);
        }

        public static bool operator <(Size my, Size other)
        {
            return (my.Width < other.Width && my.Height < other.Height);
        }
    }
}
