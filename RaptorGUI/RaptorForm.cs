﻿using System;
using Raptor;
using Raptor.Api;
using Raptor.Api.Hooks;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RaptorGUI.Extensions;
using Terraria;

namespace RaptorGUI
{
	/// <summary>
	/// Represents a form displayed on screen.
	/// </summary>
	public class RaptorForm : IDisposable
	{
		#region Members and Properties
		/// <summary>
		/// Position of the form onscreen. (X, Y)
		/// </summary>
		public Point Position;
		
		/// <summary>
		/// Size of the form. (Width, Height)
		/// </summary>
		public Size Size;
		
		/// <summary>
		/// The complete area of the form.
		/// </summary>
		public Rectangle Area
		{
			get
			{
				return new Rectangle(Position.X, Position.Y, Size.Width, Size.Height);
			}
		}
		
		/// <summary>
		/// All controls contained within this form.
		/// </summary>
		public List<RaptorControl> Children = new List<RaptorControl>();
		
		/// <summary>
		/// Invoked when the form is clicked.
		/// </summary>
		public event EventHandler<MouseClickEventArgs> FormClick;
		
		/// <summary>
		/// Dictates wether the form should automatically resize with control changes.
		/// </summary>
		public bool AutoResize = false;
		
		/// <summary>
		/// Background color of the form.
		/// </summary>
		public Color BackgroundColor = new Color(100, 100, 100, 200);

        /// <summary>
        /// Represents the height of the menu bar which includes
        /// the close button.
        /// </summary>
	    protected int MenuBarHeight = 25;

        /// <summary>
        /// The title of the form is displayed in its menubar when the form is shown.
        /// </summary>
	    public string Title = "";

        /// <summary>
        /// If the object is minimized, only the menu bar will be drawn.
        /// </summary>
	    public bool Minimized = false;

        /// <summary>
        /// If true then the close button will close the form on click.
        /// </summary>
	    public bool EnableCloseButton = false;

        /// <summary>
        /// If true then a form may be minimized when the minimize button is clicked.
        /// </summary>
	    public bool EnableMinimizeButton = true;

        /// <summary>
        /// The area of the minimize window button.
        /// </summary>
	    protected Rectangle MinimizeButtonArea;

        /// <summary>
        /// The area of the close window button.
        /// </summary>
        protected Rectangle CloseButtonArea;

        /// <summary>
        /// The are of the title text of the form.
        /// Not used for anything but may be necessary in a subclass.
        /// </summary>
        protected Rectangle TitleArea;

	    private bool _clickedInsideMenuBar = false;
		#endregion
		
		#region Constructor
		/// <summary>
		/// Constructor for RaptorForm.
		/// </summary>
		/// <param name="position">The intital position of the form.</param>
		/// <param name="size">The initial size of the form.</param>
		public RaptorForm(Point position, Size size)
		{
			Position = position;
			Size = size;
			GameHooks.Draw["Interface"] += Draw;
			GuiFramework.MouseClick += OnMouseClick;
            GuiFramework.MouseMove += OnMouseMove;
		    FormClick += HandleFormClick;
		}
	    #endregion
		
		#region Destructor
		/// <summary>
		/// Destructor for RaptorForm.
		/// </summary>
		~RaptorForm()
		{
			Dispose();
		}
		#endregion
		
		#region MouseEventHandlers
        private void OnMouseMove(object sender, MouseMoveEventArgs e)
        {
            if (Input.MouseLeftDown)
            {
                if (_clickedInsideMenuBar)
                {
                    Position.X += e.DeltaX;
                    Position.Y += e.DeltaY;
                }
            }
            else
            {
                if (_clickedInsideMenuBar)
                    _clickedInsideMenuBar = false;
            }
        }

		private void OnMouseClick(object o, MouseClickEventArgs e)
		{
			if (Area.Contains(e.X, e.Y))
				if (FormClick != null)
    				FormClick(o, e);
		}

	    private void HandleFormClick(object o, MouseClickEventArgs e)
	    {
	        if (CloseButtonArea.Contains(e.X, e.Y))
	        {
	            if (EnableCloseButton)
	            {
	                Close();
	            }
	        }
	        else if (MinimizeButtonArea.Contains(e.X, e.Y))
	        {
	            if (EnableMinimizeButton)
	            {
	                Minimized = !Minimized;
	            }
	        }

	        var menuBarArea = new Rectangle(Area.X, Area.Y, Area.Width, MenuBarHeight);
	        if (menuBarArea.Contains(e.X, e.Y))
	        {
	            _clickedInsideMenuBar = true;
	        }
	    }
		#endregion
		
		#region Draw
		/// <summary>
		/// Displays the form onscreen.
		/// </summary>
		public virtual void Draw(object o, GameHooks.DrawEventArgs e)
		{
		    var areaToDraw = Area;
		    areaToDraw.Height += MenuBarHeight;
            //Draw background.
		    if (Minimized)
		    {
		        areaToDraw = new Rectangle(Area.X, Area.Y, Area.Width, MenuBarHeight);
		    }
		    e.SpriteBatch.DrawGuiRectangle(areaToDraw, BackgroundColor);
            //Draw menu bar.
            e.SpriteBatch.DrawGuiLine(
                new Vector2(areaToDraw.X,areaToDraw.Y+MenuBarHeight), 
                new Vector2(areaToDraw.X + areaToDraw.Width, areaToDraw.Y+MenuBarHeight), 
                new Color(200,200,200,200)
                );

		    var closeTextSize = Main.fontMouseText.MeasureString("X");
		    var titleTextSize = Main.fontMouseText.MeasureString(Title);
		    var minimizeTextSize = Main.fontMouseText.MeasureString("__");
		    var closePosition = new Vector2(areaToDraw.X + areaToDraw.Width - closeTextSize.X-3, areaToDraw.Y+MenuBarHeight/2-closeTextSize.Y/2);
		    var titlePosition = new Vector2(areaToDraw.X + areaToDraw.Width/2 - titleTextSize.X/2, areaToDraw.Y+MenuBarHeight/2-titleTextSize.Y/2);
            var minimizePosition = new Vector2(areaToDraw.X + areaToDraw.Width - closeTextSize.X - 6 - minimizeTextSize.X, areaToDraw.Y + MenuBarHeight / 2 - closeTextSize.Y / 2);

		    MinimizeButtonArea = new Rectangle((int)minimizePosition.X, (int)minimizePosition.Y, (int)minimizeTextSize.X, (int)minimizeTextSize.Y);
		    TitleArea = new Rectangle((int)titlePosition.X, (int)titlePosition.Y, (int)titleTextSize.X, (int)titleTextSize.Y);
		    CloseButtonArea = new Rectangle((int)closePosition.X, (int)closePosition.Y, (int)closeTextSize.X, (int)closeTextSize.Y);

            e.SpriteBatch.DrawGuiText("X", closePosition, new Color(255,255,255,255));
            e.SpriteBatch.DrawGuiText("__", minimizePosition, new Color(255, 255, 255, 255));
            e.SpriteBatch.DrawGuiText(Title, titlePosition, new Color(255,255,255,255));

            //Draw controls.
		    if (!Minimized)
		    {
		        foreach (RaptorControl control in Children)
		        {
		            control.Draw(o, e);
		        }
		    }
		}
		#endregion
		
		#region AddControl
		/// <summary>
		/// Adds the specified control to this form's children.
		/// </summary>
		/// <param name="control">The new control to be made into a child.</param>
		/// <param name="pos">The position of the child relative to the form.</param>
		public void AddControl(RaptorControl control, Point pos)
		{
			control.Parent = this;
			//pos = Vector2.Clamp(pos, Position, Vector2.Add(Size, Position));
			control.RelativePosition = pos;
			Children.Add(control);
			
			if (AutoResize)
				ResizeToFit();
		}
		#endregion
		
		#region ResizeToFit
		/// <summary>
		/// Resizes the form to fit all of the controls within it.
		/// </summary>
		public void ResizeToFit()
		{
            Size max = new Size(0, 0);
			foreach(RaptorControl con in Children)
			{
				if ((Math.Abs(con.Area.Right-con.Area.X)) > max.Width)
					max.Width = con.Area.Right;
				if ((Math.Abs(con.Area.Bottom-con.Area.Y)) > max.Height)
					max.Height = con.Area.Bottom;
			}
			
			Size = max;
		}
		#endregion
		
		#region GetVectorDifference
		/// <summary>
		/// Gets the difference between the vector of the current position of this form and 
		/// the specified other vector.
		/// </summary>
		/// <param name="other">The vector to compare against this form's current position.</param>
		/// <returns>The difference between this form's position and <paramref name="other"/></returns>
		internal Vector2 GetVectorDifference(Vector2 other)
		{
			return new Vector2(Math.Abs(Position.X - other.X), Math.Abs(Position.Y - other.Y));
		}
		#endregion
		
		#region Dispose
	    public void Close()
	    {
	        Dispose();
	    }

		public void Dispose()
		{
		    foreach (RaptorControl ctl in Children)
		    {
		        ctl.Dispose();
		    }
		    FormClick = null;
			GameHooks.Draw["Interface"] -= Draw;
            GuiFramework.MouseMove -= OnMouseMove;
		    GuiFramework.ActiveForms.Remove(this);
		}
		#endregion
	}
}
