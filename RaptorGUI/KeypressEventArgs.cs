﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace RaptorGUI
{
	/// <summary>
	/// Event args for keypresses.
	/// </summary>
	public class KeypressEventArgs : EventArgs
	{		
		/// <summary>
		/// The key pressed by the player.
		/// </summary>
		public Keys[] KeysPressed;
		
		/// <summary>
		/// If the Ctrl (Control) key was also held.
		/// </summary>
		public bool Control = false;
		
		/// <summary>
		/// If the Shift key was also held.
		/// </summary>
		public bool Shift = false;
		
		/// <summary>
		/// If the Alt key was also held.
		/// </summary>
		public bool Alt = false;
	}
}
