﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RaptorGUI.Extensions
{
    public static class PointExt
    {
        public static Point Add(this Point self, Point other)
        {
            return new Point(self.X + other.X, self.Y + other.Y);
        }

        public static Vector2 ToVector2(this Point self)
        {
            return new Vector2(self.X, self.Y);
        }
    }
}
