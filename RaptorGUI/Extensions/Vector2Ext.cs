﻿using System;
using Microsoft.Xna.Framework;

namespace RaptorGUI.Extensions
{
	public static class Vector2Ext
	{
		/// <summary>
		/// Returns this vector as a <see cref="T:Microsoft.Xna.Framework.Point"/>.
		/// </summary>
		/// <returns>This vector as a Point.</returns>
		public static Point ToPoint(this Vector2 vec)
		{
			return new Point((int)vec.X, (int)vec.Y);
		}

	    public static Size ToSize(this Vector2 vec)
	    {
	        return new Size((int) vec.X, (int) vec.Y);
	    }
	}
}
