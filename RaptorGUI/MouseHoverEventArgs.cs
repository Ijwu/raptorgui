﻿using System;
using Microsoft.Xna.Framework;

namespace RaptorGUI
{
	/// <summary>
	/// Event arts for when the mouse hovers in place..
	/// </summary>
	public class MouseHoverEventArgs : EventArgs
	{
		/// <summary>
		/// Current position of the mouse cursor.
		/// </summary>
		public Vector2 Position
		{
			get
			{
				return new Vector2(X, Y);
			}
		}
		
		/// <summary>
		/// X-coordinate of the cursor
		/// </summary>
		public int X;
		
		/// <summary>
		/// Y-coordinate of the cursor
		/// </summary>
		public int Y;
		
		/// <summary>
		/// Amount of time in milliseconds in which the cursor has been
		/// hovering in the same spot.
		/// </summary>
		public int Time;
		
		/// <summary>
		/// Constructor for MouseHoverEventArgs.
		/// </summary>
		/// <param name="x">X-coordinate of the mouse cursor.</param>
		/// <param name="y">Y-coordinate of the mouse cursor.</param>
		/// <param name="time">Amount of time in milliseconds the cursor has been paused and hovering.</param>
		public MouseHoverEventArgs(int x, int y, int time)
		{
			X = y;
			Y = y;
			Time = time;
		}
	}
}
