﻿using System;
using Raptor;
using Raptor.Api;
using Raptor.Api.Hooks;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RaptorGUI
{
	/// <summary>
	/// Event args for GUI form clicks in-game.
	/// </summary>
	public class MouseClickEventArgs : EventArgs
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		public MouseClickEventArgs() {}
		
		/// <summary>
		/// Constructs the args using the vector given as the point that was clicked.
		/// </summary>
		/// <param name="pos">The point that was clicked.</param>
		public MouseClickEventArgs(Vector2 pos)
		{
			X = (int)pos.X;
			Y = (int)pos.Y;
		}
		
		/// <summary>
		/// Constructs the args using the coordinates given.
		/// </summary>
		/// <param name="x">X-Coordinate of the clicked point.</param>
		/// <param name="y">Y-Coordinate of the clicked point.</param>
		public MouseClickEventArgs(int x, int y)
		{
			X = x;
			Y = y;
		}
		
		/// <summary>
		/// X coordinate of the click relative to the form.
		/// </summary>
		public int X;
		
		/// <summary>
		/// Y coordinate of the click relative to the form.
		/// </summary>
		public int Y;
		
		/// <summary>
		/// Vector of the click relative to the form.
		/// </summary>
		public Vector2 Position
		{
			get
			{
				return new Vector2(X, Y);
			}
		}
	}
	
}
