﻿using System;
using Raptor;
using Terraria;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RaptorGUI.Controls
{
	/// <summary>
	/// Represents text to be displayed in a form.
	/// </summary>
	public class Button : RaptorControl
	{
		private string text;
		private Vector2 textPosition;
		
		/// <summary>
		/// The text to be displayed in the center of the button.
		/// </summary>
		public string Text
		{
			get
			{
				return text;
			}
			set
			{
				text = value;
				ResizeForText();
			}
		}
		
		/// <summary>
		/// The color in which to render the text in the center of the button.
		/// </summary>
		public Color TextColor = Color.White;
		
		/// <summary>
		/// The color in which to render the button area.
		/// </summary>
		public Color BackgroundColor = new Color(100, 100, 100, 200);
		private Color bufferColor;
		
		/// <summary>
		/// The font in which to render the button text.
		/// </summary>
		public SpriteFont Font = Main.fontMouseText;
		
		/// <summary>
		/// The texture to use for the button area.
		/// Will default to Raptor's if unassigned.
		/// </summary>
		public Texture2D ButtonTexture = null;
		
		/// <summary>
		/// The space between the borders of the button area and the text.
		/// </summary>
		public int TextPadding = 20;
		
		#region Constructors
		/// <summary>
		/// Constructor for Button.
		/// </summary>
		/// <param name="text">The button text.</param>
		public Button(string text)
		{
			this.text = text;
			
			SetParent += OnSetParent;
			MouseEnter += OnMouseEnter;
			MouseExit += OnMouseExit;
		}
		
		/// <summary>
		/// Constructor for Button.
		/// </summary>
		/// <param name="text">The button text.</param>
		/// <param name="font">The font in which to render the button text.</param>
		public Button(string text, SpriteFont font)
		{
			this.text = text;
			Font = font;
			
			SetParent += OnSetParent;
			MouseEnter += OnMouseEnter;
			MouseExit += OnMouseExit;
		}
		
		/// <summary>
		/// Constructor for Button
		/// </summary>
		/// <param name="text">The button text.</param>
		/// <param name="size">The size of the button.</param>
		/// <remarks>Will not automatically resize for the text when constructed.</remarks>
		public Button(string text, Size size)
		{
			this.text = text;
			Size = size;
			
			SetParent += OnSetParent;
			MouseEnter += OnMouseEnter;
			MouseExit += OnMouseExit;
		}
		
		/// <summary>
		/// Constructor for Button
		/// </summary>
		/// <param name="text">The button text.</param>
		/// <param name="size">The size of the button.</param>
		/// <param name="padding">The space between the button text and button borders.</param>
		/// <remarks>Will not automatically resize for the text when constructed.</remarks>
		public Button(string text, Size size, int padding)
		{
			this.text = text;
			Size = size;
			TextPadding = padding;
			
			SetParent += OnSetParent;
			MouseEnter += OnMouseEnter;
			MouseExit += OnMouseExit;
		}
		#endregion
		
		/// <summary>
		/// Resizes the button in order to accomodate for changes in its text.
		/// </summary>
		private void ResizeForText()
		{
			Vector2 size = Font.MeasureString(text);
			textPosition = new Vector2(Area.Center.X-(size.X/2), Area.Center.Y-(size.Y/2));
			Size = new Size((int)size.X+TextPadding*2, (int)size.Y+TextPadding*2);
		}
		
		public override void Draw(object o, Raptor.Api.Hooks.GameHooks.DrawEventArgs e)
		{
			ResizeForText(); //TODO: This doesn't work in OnSetParent correctly. Fix it.
			e.SpriteBatch.DrawGuiRectangle(Area, BackgroundColor, ButtonTexture);
			e.SpriteBatch.DrawString(Font, Text, textPosition, TextColor);
		}

		private void OnMouseEnter(object o, EventArgs e)
		{
			bufferColor = BackgroundColor;
			BackgroundColor = new Color(BackgroundColor.R+50, BackgroundColor.G+50, BackgroundColor.B+50);
		}
		
		private void OnMouseExit(object o, EventArgs e)
		{
			BackgroundColor = bufferColor;
		}
		
		private void OnSetParent(object o, EventArgs e)
		{
			ResizeForText();
		}
	}
}
