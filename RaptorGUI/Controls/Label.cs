﻿using System;
using RaptorGUI.Extensions;
using Terraria;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RaptorGUI.Controls
{
	/// <summary>
	/// Represents text to be displayed in a form.
	/// </summary>
	public class Label : RaptorControl
	{
		private string text;
		
		/// <summary>
		/// The text of the label.
		/// </summary>
		public string Text
		{
			get
			{
				return text;
			}
			set
			{
				text = value;
				Size = Font.MeasureString(value).ToSize();
			}
		}
		
		/// <summary>
		/// The color of the text.
		/// </summary>
		public Color TextColor = Color.White;
		
		/// <summary>
		/// The font in which to render the text.
		/// </summary>
		public SpriteFont Font;
		
		/// <summary>
		/// Constructor for Label
		/// </summary>
		/// <param name="text">The text that the label will portray.</param>
		public Label(string text)
		{
			this.text = text;
			Font = Main.fontMouseText;
			Size = Font.MeasureString(text).ToSize();
		}
		
		/// <summary>
		/// Constructor for Label
		/// </summary>
		/// <param name="text">The text that the label will portray.</param>
		/// <param name="font">The font in which to render the text.</param>
		public Label(string text, SpriteFont font)
		{
			this.text = text;
			Font = font;
			Size = font.MeasureString(text).ToSize();
		}
		
		public override void Draw(object o, Raptor.Api.Hooks.GameHooks.DrawEventArgs e)
		{
			e.SpriteBatch.DrawString(Font, Text, Position.ToVector2(), TextColor);
		}
	}
}
