﻿using System;
using Microsoft.Xna.Framework.Input;
using Raptor;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Raptor.Api.Hooks;
using Terraria;

namespace RaptorGUI.Controls
{
	/// <summary>
	/// Control used to receive text input from the player.
	/// </summary>
	public class InputBox : RaptorControl
    {
        #region Members and Properties
        private string text = "";
		private Vector2 textPosition;

        /// <summary>
        /// The InputBox will expand as text is entered, but will not grow in length
        /// passed this amount of characters.
        /// </summary>
	    public int FixedWidth = 20;

		/// <summary>
		/// The space between the walls of the InputBox and the text.
		/// </summary>
		public int TextPadding = 4;
		
		/// <summary>
		/// The current text entered in the box.
		/// </summary>
		public string Text
		{
			get
			{
				return text;
			}
			set
			{
				text = value;
			}
		}
		
        /// <summary>
        /// The text that is displayed in the box when no text has been entered.
        /// </summary>
		public string DefaultText = "";
		
		/// <summary>
		/// The font in which to render the text.
		/// </summary>
		public SpriteFont Font = Main.fontMouseText;
		
		/// <summary>
		/// The color of the text.
		/// </summary>
		public Color TextColor = Color.White;
		
		/// <summary>
		/// Background color of the input box.
		/// </summary>
		public Color BackgroundColor = new Color(100, 100, 100, 200);
        #endregion

        #region Constructors
        /// <summary>
		/// Constructor for InputBox.
		/// </summary>
		/// <param name="defaultText">Text to be displayed by default when the InputBox has no value and is out of focus.</param>
		public InputBox(string defaultText)
	    {
			DefaultText = defaultText;
            Width = (int)Font.MeasureString(defaultText).X;

            GameHooks.Update += OnUpdate;
	        SetParent += OnSetParent;
	    }
		
		/// <summary>
		/// Constructor for InputBox.
		/// </summary>
		/// <param name="defaultText">Text to be displayed by default when the InputBox has no value and is out of focus.</param>
		/// <param name="textColor">The color to display the entered text in.</param>
		public InputBox(string defaultText, Color textColor)
		{
            DefaultText = defaultText;
			TextColor = textColor;
            Width = (int)Font.MeasureString(defaultText).X;

            GameHooks.Update += OnUpdate;
		    SetParent += OnSetParent;
		}
		
		/// <summary>
		/// Constructor for InputBox.
		/// </summary>
		/// <param name="defaultText">Text to be displayed by default when the InputBox has no value and is out of focus.</param>
		/// <param name="textColor">The color to display the entered text in.</param>
		/// <param name="backgroundColor">The background color of the control.</param>
		public InputBox(string defaultText, Color textColor, Color backgroundColor)
		{
			DefaultText = defaultText;
			TextColor = textColor;
			BackgroundColor = backgroundColor;
            Width = (int)Font.MeasureString(defaultText).X;

            GameHooks.Update += OnUpdate;
		    SetParent += OnSetParent;
		}

        /// <summary>
        /// Constructor for InputBox.
        /// </summary>
        /// <param name="defaultText">Text to be displayed by default when the InputBox has no value and is out of focus.</param>
        /// <param name="textColor">The color to display the entered text in.</param>
        /// <param name="backgroundColor">The background color of the control.</param>
        /// <param name="font">Font used to render the text.</param>
        public InputBox(string defaultText, Color textColor, Color backgroundColor, SpriteFont font)
        {
            DefaultText = defaultText;
            TextColor = textColor;
            BackgroundColor = backgroundColor;
            Font = font;
            Width = (int)Font.MeasureString(defaultText).X;

            GameHooks.Update += OnUpdate;
            SetParent += OnSetParent;
        }
        #endregion

        #region ResizeForText
        private void ResizeForText()
	    {
	        string txt = Text != string.Empty ? Text : DefaultText;

            Vector2 size = Font.MeasureString(txt);
            textPosition = new Vector2(Area.Center.X - (size.X / 2), Area.Center.Y - (size.Y / 2));
            Size = new Size((int)size.X + TextPadding * 2, (int)size.Y + TextPadding * 2);
	    }
        #endregion

        #region Draw
        public override void Draw(object o, GameHooks.DrawEventArgs e)
		{
            ResizeForText();
			e.SpriteBatch.DrawGuiRectangle(Area, BackgroundColor);
			e.SpriteBatch.DrawString(Font, Text != string.Empty ? Text : DefaultText, textPosition, TextColor);
		}
        #endregion

        #region EventHandlers
        private void OnSetParent(object o, EventArgs e)
	    {
	        ResizeForText();
	    }

	    private void OnUpdate(object o, GameHooks.UpdateEventArgs e)
	    {
	        if (Focus)
	        {
	            Input.DisabledKeyboard = true;
	            if (!(Text.Length >= FixedWidth))
	                Text += Input.TypedString;

	            if (Input.IsKeyTapped(Keys.Back))
	            {
	                if (!string.IsNullOrEmpty(Text))
	                    Text = Text.Substring(0, Text.Length - 1);
	            }
	        }
	        else
	        {
	            Input.DisabledKeyboard = false;
	        }
	    }
        #endregion

        #region Dispose
        public void Dispose()
	    {
            GameHooks.Update -= OnUpdate;
	        base.Dispose();
        }
        #endregion
    }
}
