﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Raptor;
using Raptor.Api;
using Raptor.Api.Hooks;
using Raptor.Api.Commands;
using RaptorGUI.Controls;
using Terraria;

namespace RaptorGUI
{
    /// <summary>
    /// Main plugin class.
    /// Holds all active forms and relays input to them.
    /// Contains functions to create new forms easily.
    /// </summary>
    [ApiVersion(1, 0)]
    public class GuiFramework : TerrariaPlugin
    {
        #region PluginDetails

        public override string Author
        {
            get { return "Ijwu"; }
        }

        public override string Description
        {
            get { return "Gooey and stuff."; }
        }

        public override string Name
        {
            get { return "GUI Framework"; }
        }

        public override Version Version
        {
            get { return new Version(1, 0, 0); }
        }

        #endregion

        #region Members and Properties

        /// <summary>
        /// Used to count how long the mouse is hovering in place for.
        /// Used to decide when to invoke MouseHover.
        /// </summary>
        private System.Diagnostics.Stopwatch mouseHoverStopwatch = new System.Diagnostics.Stopwatch();

        private Vector2 previousMousePosition = new Vector2(0, 0);

        /// <summary>
        /// Currently active forms.
        /// </summary>
        public static List<RaptorForm> ActiveForms = new List<RaptorForm>();

        #endregion

        #region Events

        /// <summary>
        /// Invoked when the left mouse button is clicked.
        /// </summary>
        public static event EventHandler<MouseClickEventArgs> MouseClick;

        /// <summary>
        /// Invoked when the mouse is moved.
        /// Don't depend on this for anything critical.
        /// The event isn't entirely reliable.
        /// </summary>
        public static event EventHandler<MouseMoveEventArgs> MouseMove;

        /// <summary>
        /// Invoked when the mouse stops moving and hovers in place.
        /// </summary>
        public static event EventHandler<MouseHoverEventArgs> MouseHover;

        /// <summary>
        /// Invoked when the user presses down a key on their keyboard.
        /// </summary>
        public static event EventHandler<KeypressEventArgs> Keypress;

        #endregion

        #region Initialize

        /// <summary>
        /// Initializes the mouse update handler.
        /// </summary>
        public override void Initialize()
        {
            GameHooks.Update += OnUpdateMouseHandler;
            GameHooks.Update += OnUpdateKeyboardHandler;

            //Keypress += delegate(object sender, KeypressEventArgs e)
            //{
            //    var x = new System.Text.StringBuilder();
            //    foreach (Keys k in e.KeysPressed)
            //    {
            //        x.Append(string.Format("{0} ", k.ToString()));
            //    }

            //    Utils.NewSuccessText("{0} -- Ctrl: {1}, Shift {2}, Alt {3}",
            //        x.ToString(),
            //        e.Control,
            //        e.Shift,
            //        e.Alt);
            //};

            Commands.Register(new Command(Test, "testgui"));
            Commands.Register(new Command(Test2, "testgui2"));
            Commands.Register(new Command(Test3, "testgui3"));
            Commands.Register(new Command(UndoTest, "undogui"));
        }

        #endregion

        #region AddForm

        /// <summary>
        /// Proxy method to add a form to the list of active forms.
        /// Expects a pre-made form.
        /// </summary>
        /// <param name="form">The form to be added.</param>
        public static void AddForm(RaptorForm form)
        {
            GuiFramework.ActiveForms.Add(form);
        }

        #endregion

        #region TestCommands

        //TODO: Fix clamping controls so they're forced to be in the form.
        //TODO: Fix resizing of forms.
        //TODO: Add form exit button (which can be disabled) and allow for moving of forms. (Maybe resizing?)
        //TODO: Make more test forms and assess usability.
        //TODO: Layouts?
        //TODO: Allow input boxes to not have default text, causing them to be a static size until text is entered
        //TODO: Make a password input box class
        //TODO: Make a close/cancel/ok button class?
        //TODO: Keyboard isn't properly locked with input boxes.
        //TODO: Button in test1.
        //TODO: Add FormClose event.

        public void Test(object o, CommandEventArgs e)
        {
            RaptorForm test = new RaptorForm(new Point(100, 100), new Size(400, 300));
            InputBox nameInput = new InputBox("Enter Name Here");
            InputBox passInput = new InputBox("Enter Password Here");
            Button confirmButton = new Button("Ok", new Size(50, 25));

            //test.AddControl(new Label("Registration Form"), new Point(100, 25));
            test.AddControl(new Label("Name:"), new Point(25, 100));
            test.AddControl(nameInput, new Point(150, 100));
            test.AddControl(new Label("Password:"), new Point(25, 150));
            test.AddControl(passInput, new Point(150, 150));
            test.AddControl(confirmButton, new Point(400-confirmButton.Width, 300-confirmButton.Height));

            confirmButton.ControlClick += (sender, args) =>
            {
                Raptor.Utils.NewSuccessText("Username: {0}", nameInput.Text);
                Raptor.Utils.NewSuccessText("Password: {0}", passInput.Text);
                test.Close();
            };

            test.Title = "Registration Form";

            AddForm(test);
        }

        public void Test2(object o, CommandEventArgs e)
        {
            RaptorForm test = new RaptorForm(new Point(100, 100), new Size(400, 400));
            Controls.Button s = new Controls.Button("Testing");
            s.ControlClick += delegate { Raptor.Utils.NewSuccessText("GAH. Don't do that. It hurts."); };
            s.TextPadding = 20;

            ActiveForms.Clear();
            test.AddControl(s, new Point(100, 100));
            AddForm(test);
        }

        public void Test3(object o, CommandEventArgs e)
        {
            RaptorForm test = new RaptorForm(new Point(100, 100), new Size(400, 400));
            Controls.InputBox x = new InputBox("I am Empty", Color.DarkMagenta, Color.LightSteelBlue);

            ActiveForms.Clear();
            test.AddControl(x, new Point(100, 100));
            AddForm(test);
        }

        public void UndoTest(object o, CommandEventArgs e)
        {
            for (int i = 0; i < ActiveForms.Count; i++)
                ActiveForms[i].Dispose();
        }

        #endregion

        #region OnUpdateKeyboardHandler
        /// <summary>
        /// Checks keyboard input each update and invokes events when applicable.
        /// </summary>
        public void OnUpdateKeyboardHandler(object o, GameHooks.UpdateEventArgs e)
        {
            if (Main.gameMenu)
                return;

            KeypressEventArgs args = new KeypressEventArgs()
            {
                KeysPressed = Input.GetPressedKeys(),
                Control = Input.Control,
                Shift = Input.Shift,
                Alt = Input.Alt
            };

            if (Keypress != null)
                Keypress(this, args);
        }
        #endregion

        #region OnUpdateMouseHandler

        /// <summary>
        /// Checks mouse input every update and invokes events when applicable.
        /// </summary>
        private void OnUpdateMouseHandler(object o, GameHooks.UpdateEventArgs e)
        {
            if (Main.gameMenu)
                return;

            if (Input.DisabledMouse)
                return;

            Vector2 buffer = new Vector2(Input.MouseX, Input.MouseY);

            if (buffer == previousMousePosition)
            {
                if (!mouseHoverStopwatch.IsRunning)
                    mouseHoverStopwatch.Restart();
                if (MouseHover != null)
                    MouseHover(o,
                        new MouseHoverEventArgs(Input.MouseX, Input.MouseY,
                            (int) mouseHoverStopwatch.ElapsedMilliseconds));
            }
            else
            {
                mouseHoverStopwatch.Reset();
                previousMousePosition = buffer;
            }

            if (Input.MouseDX != 0 || Input.MouseDY != 0)
                if (MouseMove != null)
                    MouseMove(o, new MouseMoveEventArgs(Input.MouseDX, Input.MouseDY));

            if (Input.MouseLeftClick)
                if (MouseClick != null)
                    MouseClick(o, new MouseClickEventArgs(Input.MouseX, Input.MouseY));
        }

        #endregion

        #region Dispose

        /// <summary>
        /// Disposes of the mouse update handler.
        /// </summary>
        /// <param name="disposing">True if disposing.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                GameHooks.Update -= OnUpdateMouseHandler;
            }
            base.Dispose(disposing);
        }

        #endregion
    }
}