﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Raptor;
using Raptor.Api.Hooks;
using RaptorGUI.Extensions;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RaptorGUI
{		
	/// <summary>
	/// A control which is contained and controlled in a RaptorForm.
	/// </summary>
	public abstract class RaptorControl : IDisposable
	{
		#region Members and Properties
		/// <summary>
		/// The position of the control relative to its parent form. (X, Y)
		/// </summary>
		public Point RelativePosition;
		
		/// <summary>
		/// The position on screen in which the control is. (X, Y)
		/// </summary>
        public Point Position
		{
			get
			{
				return RelativePosition.Add(Parent.Position);
			}
		}
		
		/// <summary>
		/// The size of the control. (Width, Height)
		/// </summary>
		public Size Size = new Size(0,0);

        /// <summary>
        /// Gets or sets the control's width.
        /// </summary>
	    public int Width
	    {
	        get
	        {
	            return Size.Width;
	        }
	        set
	        {
	            Size.Width = value;
	        }
	    }

        /// <summary>
        /// Gets or sets the control's height.
        /// </summary>
	    public int Height
	    {
	        get
	        {
	            return Size.Height;
	        }
	        set
	        {
	            Size.Height = value;
	        }
	    }
		
		private RaptorForm _parent;
		
		/// <summary>
		/// The parent form of the control.
		/// </summary>
		public RaptorForm Parent
		{
			get
			{
				return _parent;
			}
			set
			{
				_parent = value;
				if (SetParent != null)
					SetParent(this, new EventArgs());
			}
		}
		
		/// <summary>
		/// The complete area of the control on screen.
		/// </summary>
		public Rectangle Area
		{
			get
			{
				return new Rectangle(Position.X, Position.Y, Size.Width, Size.Height);
			}
		}	
		
		/// <summary>
		/// How long the mouse should hover in place over a control before the
		/// MouseHover event is invoked.
		/// </summary>
		public int MouseHoverTime = 500;
		
		/// <summary>
		/// Dictates whether the mouse is currently within the control.
		/// </summary>
		protected bool MouseCurrentlyInside = false;
		
		/// <summary>
		/// Whether this control has focus or not.
		/// </summary>
		protected bool Focus = false;
		#endregion
		
		#region Constructor
		/// <summary>
		/// Default Constructor for RaptorControl
		/// </summary>
		public RaptorControl()
		{
			GuiFramework.MouseMove += OnMouseMove;
			GuiFramework.MouseHover += OnMouseHover;
			ControlClick += OnControlClick;
            SetParent += OnSetParent;
		}
	    #endregion
		
		#region Destructor
		~RaptorControl()
		{
			Dispose();
		}
		#endregion
		
		#region MouseEventHandlers
		
		private void OnControlClick(object o, MouseClickEventArgs e)
		{
			Focus = true;
		}
		
		private void OnMouseMove(object o, MouseMoveEventArgs e)
		{
			if (Area.Contains(e.CurrentPosition.ToPoint()))
			{
				if (!MouseCurrentlyInside)
					if (MouseEnter != null)
						MouseEnter(this, new EventArgs());
				MouseCurrentlyInside = true;
			}
			else
			{
				if (MouseCurrentlyInside)
					if (MouseExit != null)
						MouseExit(this, new EventArgs());
				MouseCurrentlyInside = false;
			}
		}
		
		private void OnMouseClick(object o, MouseClickEventArgs e)
		{
			if (Area.Contains(e.X, e.Y))
			{
			    if (ControlClick != null)
			        ControlClick(o, e);
			}
			else
			{
				Focus = false;
			}
		}
		
		private void OnMouseHover(object o, MouseHoverEventArgs e)
		{
			if (!Area.Contains(e.X, e.Y))
				return;
			
			if (e.Time > MouseHoverTime)
				if (MouseHover != null)
					MouseHover(o, e);
		}
		#endregion

        #region OnSetParent

        private void OnSetParent(object o, EventArgs e)
        {
            Parent.FormClick += OnMouseClick;
        }
        #endregion

        #region InvokeControlClick
        /// <summary>
		/// Invokes this control's ControlClick event.
		/// </summary>
		/// <param name="o">Sending object. (Ignored)</param>
		/// <param name="e">Event arguments.</param>
		internal void InvokeControlClick(object o, MouseClickEventArgs e)
		{
			ControlClick(o, new MouseClickEventArgs() { X = e.X, Y = e.Y });
		}
		#endregion
		
		#region Draw
		/// <summary>
		/// Draws the control onscreen.
		/// </summary>
		/// <param name="o">Sending object. (Ignored)</param>
		/// <param name="e">Event arguments.</param>
		public abstract void Draw(object o, GameHooks.DrawEventArgs e);
		#endregion	
		
		#region Events
		/// <summary>
		/// Invoked when a click falls through to this control.
		/// </summary>
		public event EventHandler<MouseClickEventArgs> ControlClick;
		
		/// <summary>
		/// Invoked when the control's parent is set.
		/// </summary>
		public event EventHandler SetParent;
		
		/// <summary>
		/// Invoked when the mouse enters the control.
		/// </summary>
		public event EventHandler MouseEnter;
		
		/// <summary>
		/// Invoked when the mouse leaves the control.
		/// </summary>
		public event EventHandler MouseExit;
		
		/// <summary>
		/// Invoked when the mouse pauses over the control.
		/// </summary>
		public event EventHandler<MouseHoverEventArgs> MouseHover;
		#endregion
		
		#region Dispose
		public void Dispose()
		{
			GuiFramework.MouseClick -= OnMouseClick;
			GuiFramework.MouseMove -= OnMouseMove;
			GuiFramework.MouseHover -= OnMouseHover;
		    MouseEnter = null;
		    MouseExit = null;
		    ControlClick = null;
		    MouseHover = null;
            SetParent = null;
		}
	    #endregion
	}
}
