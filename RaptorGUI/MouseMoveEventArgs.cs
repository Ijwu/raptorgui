﻿using System;
using Raptor;
using Raptor.Api;
using Raptor.Api.Hooks;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RaptorGUI
{
	/// <summary>
	/// Event args for mouse movements in-game.
	/// </summary>
	public class MouseMoveEventArgs : EventArgs
	{
		/// <summary>
		/// The position onscreen the mouse was in previous to this movement.
		/// </summary>
		public Vector2 PreviousPosition
		{
			get
			{
				return new Vector2(Input.MouseX+DeltaX, Input.MouseY+DeltaY);
			}
		}
		
		/// <summary>
		/// The current onscreen position of the mouse, after the movement.
		/// </summary>
		public Vector2 CurrentPosition
		{
			get 
			{
				return new Vector2(Input.MouseX, Input.MouseY);
			}
		}
		
		/// <summary>
		/// The delta value of the mouse's x-coordinate after this movement.
		/// </summary>
		public int DeltaX;
		
		/// <summary>
		/// The delta value of the mouse's y-coordinate after this movement.
		/// </summary>
		public int DeltaY;
		
		/// <summary>
		/// Constructor for OnMouseMoveEventArgs
		/// </summary>
		/// <param name="dx">x-coordinate delta value</param>
		/// <param name="dy">y-coordinate delta value</param>
		public MouseMoveEventArgs(int dx, int dy)
		{
			DeltaX = dx;
			DeltaY = dy;
		}
	}
}
